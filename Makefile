# SPDX-License-Identifier: Apache-2.0

.PHONY: build
build:
	docker image build -t quay.io/subprovisioner/subprovisioner:0.1.0 .

.PHONY: fmt
fmt:
	go fmt ./...

.PHONY: vet
vet:
	go vet ./...
