<!-- ----------------------------------------------------------------------- -->

# Subprovisioner

A CSI plugin for Kubernetes that allows provisioning `Block` volumes from an
existing `Filesystem` volume.

<!-- ----------------------------------------------------------------------- -->

## How to install

To install the latest release:

```console
$ kubectl apply -f https://gitlab.com/subprovisioner/subprovisioner/-/raw/v0.1.0/deployment.yaml
```

And to uninstall:

```console
$ kubectl delete --ignore-not-found -f https://gitlab.com/subprovisioner/subprovisioner/-/raw/v0.1.0/deployment.yaml
```

<!-- ----------------------------------------------------------------------- -->

## How it's used

> This plugin currently assumes that all nodes in the Kubernetes cluster have
> the kernel NBD client loaded. See [Limitations](#limitations) for more
> information.

Let's assume you have a `PersistentVolumeClaim` (PVC) of `Filesystem` type with
support for the `ReadWriteMany` access mode, and that it is named `backing-pvc`
and belongs to namespace `default`.

To use Subprovisioner, first create a `StorageClass`:

```yaml
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: my-storage-class
provisioner: subprovisioner.gitlab.io
parameters:
  backingClaimName: backing-pvc
  backingClaimNamespace: default
  basePath: volumes  # path in backing-pvc under which to store volumes; default is "", i.e., under the root
allowVolumeExpansion: true  # optional
```

Then provision `Block` volumes from that `StorageClass` as usual:

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: my-dynamically-provisioned-block-pvc
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Ti
  volumeMode: Block
  storageClassName: my-storage-class
```

That's it! Subprovisioner will store the volumes' data in `backing-pvc`.

Note that the volumes will initially be filled with zeroes and are thinly
provisioned, so you can specify volumes sizes bigger than the capacity of the
backing volume.

### Expanding volumes

It is possible to increase the capacity of an existing volume. To do so simply
edit the `spec.resources.requests.storage` field of the PVC. Once the volume is
expanded, `status.capacity.storage` will be updated to reflect its new size.

The volume will only be expanded once it isn't mounted in any pod.

### Cloning volumes

`Block` volumes may be provisioned by cloning other existing `Block` volumes.
This is achieved by referencing an existing PVC in the `spec.dataSource` field
of the new PVC:

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: my-cloned-pvc
spec:
  accessModes:
    - ReadWriteOnce
  dataSource:
    kind: PersistentVolumeClaim
    name: my-dynamically-provisioned-block-pvc
  resources:
    requests:
      storage: 2Ti
  volumeMode: Block
  storageClassName: my-storage-class
```

Note that you may give the cloned volume a bigger size than the original volume.
The excess size will be filled with zeroes.

The clone will be completed only once the original PVC isn't mounted in any pod.

### Snapshotting volumes

> Your Kubernetes distribution might not support volume snapshotting out of the
> box. Follow [this documentation] to manually install the volume snapshot CRDs
> and controller.

[this documentation]: https://github.com/kubernetes-csi/external-snapshotter#usage

You can create `VolumeSnapshot`s from an existing `Block` volume, to later
provision new volumes from it. In this case you will need to create a
[`VolumeSnapshotClass`] beforehand. No parameters need to be specified on it,
and a single `VolumeSnapshotClass` is enough for all Subprovisioner volumes,
even if they have different backing volumes or `StorageClass`es. This will do:

```yaml
apiVersion: snapshot.storage.k8s.io/v1
kind: VolumeSnapshotClass
metadata:
  name: subprovisioner
driver: subprovisioner.gitlab.io
deletionPolicy: Delete
```

Then create a `VolumeSnapshot` from an existing PVC like this:

```yaml
apiVersion: snapshot.storage.k8s.io/v1
kind: VolumeSnapshot
metadata:
  name: my-snapshot
spec:
  volumeSnapshotClassName: subprovisioner
  source:
    persistentVolumeClaimName: my-dynamically-provisioned-block-pvc
```

The snapshot will be completed only once the PVC isn't mounted in any pod.

You can then provision `Block` volumes from that `VolumeSnapshot`:

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: my-cloned-pvc
spec:
  accessModes:
    - ReadWriteOnce
  dataSource:
    apiGroup: snapshot.storage.k8s.io
    kind: VolumeSnapshot
    name: my-snapshot
  resources:
    requests:
      storage: 2Ti
  volumeMode: Block
  storageClassName: my-storage-class
```

Just like with volume cloning, you may give the volume a bigger size than that
of the snapshot, and the excess size will be filled with zeroes.

[`VolumeSnapshotClass`]: https://kubernetes.io/docs/concepts/storage/volume-snapshot-classes/

<!-- ----------------------------------------------------------------------- -->

## How it works

Provisioned `Block` volumes are stored in the backing `Filesystem` volume as
qcow2 image files. [qemu-storage-daemon] is used to expose those images as block
devices. Volume cloning and snapshotting work by creating overlay qcow2 files.

[qemu-storage-daemon]: https://qemu.readthedocs.io/en/latest/tools/qemu-storage-daemon.html

<!-- ----------------------------------------------------------------------- -->

## Features

- Dynamic `Block` volume provisioning.
- `ReadWriteOnce`, `ReadWriteOncePod`, and `ReadOnlyMany` access modes.
- Efficient (constant-time) offline volume expansion.
- Efficient (constant-time) offline volume cloning.
- Efficient (constant-time) offline volume snapshotting.

<!-- ----------------------------------------------------------------------- -->

## Limitations

- The plugin assumes that all nodes in the Kubernetes cluster have the kernel
  NBD client loaded, such that NBD block nodes are available at `/dev/nbd0`,
  `/dev/nbd1`, etc.

- The number of Subprovisioner-provisioned volumes that can be simultaneously
  mounted on a given node is limited by the amount of kernel NBD block devices
  that are available. Assuming the NBD kernel client was built as a module, use
  the `nbds_max` option to increase the maximum number of NBD block devices if
  needed, _e.g._, `modprobe nbd nbds_max=64`.

- The backing volume must be `ReadWriteMany`.

- You have to be careful not to delete the backing volume PVC prior to deleting
  PVCs backed by it, or else deletion of the latter will hang.

- The plugin assumes that it created all PVs that have `spec.csi.driver` set to
  `subprovisioner.gitlab.io`, so you shouldn't create such a PV manually.

- On some systems, you may need to configure Docker to allow for (bidirectional
  volume mounts)[mount-propagation].

[mount-propagation]: https://kubernetes.io/docs/concepts/storage/volumes/#configuration

<!-- ----------------------------------------------------------------------- -->

## Development

To build the `quay.io/subprovisioner/subprovisioner:0.1.0` image:

```console
$ make
```

<!-- ----------------------------------------------------------------------- -->

## License

This project is released under the Apache 2.0 license. See [LICENSE](LICENSE).

<!-- ----------------------------------------------------------------------- -->
